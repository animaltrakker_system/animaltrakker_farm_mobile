package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.GeneticCoatColor

object GeneticCoatColorTable {

    const val NAME = "genetic_coat_color_table"

    fun geneticCoatColorFromCursor(cursor: Cursor): GeneticCoatColor {
        return GeneticCoatColor(
            id = cursor.getEntityId(GeneticCoatColorTable.Columns.ID),
            registryCompanyId = cursor.getEntityId(GeneticCoatColorTable.Columns.REGISTRY_COMPANY_ID),
            color = cursor.getString(GeneticCoatColorTable.Columns.COLOR),
            colorAbbreviation = cursor.getString(GeneticCoatColorTable.Columns.ABBREVIATION),
            order = cursor.getInt(GeneticCoatColorTable.Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_geneticcoatcolorid"
        const val REGISTRY_COMPANY_ID = "id_registry_id_companyid"
        const val COLOR = "coat_color"
        const val ABBREVIATION = "coat_color_abbrev"
        const val ORDER = "coat_color_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_COAT_COLORS_BY_REGISTRY_COMPANY_ID =
            """SELECT * FROM ${GeneticCoatColorTable.NAME}
                WHERE ${GeneticCoatColorTable.Columns.REGISTRY_COMPANY_ID} = ?
                ORDER BY ${GeneticCoatColorTable.Columns.ORDER}"""
    }
}
