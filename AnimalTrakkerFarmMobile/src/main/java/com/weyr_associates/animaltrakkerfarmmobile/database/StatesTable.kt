package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.State

object StatesTable {

    const val NAME = "state_table"

    fun stateFromCursor(cursor: Cursor): State = with (cursor) {
        State(
            id = getEntityId(Columns.ID),
            name = getString(Columns.NAME),
            abbreviation = getString(Columns.ABBREVIATION),
            countryId = getEntityId(Columns.COUNTRY_ID),
            order = getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_stateid"
        const val NAME = "state_name"
        const val ABBREVIATION = "state_abbrev"
        const val COUNTRY_ID = "id_countryid"
        const val ORDER = "state_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
