package com.weyr_associates.animaltrakkerfarmmobile.database

object GeneticCharacteristicCalculationMethodTable {

    const val NAME = "genetic_characteristic_calculation_method_table"

    object Columns {
        const val ID = "id_geneticcharacteristiccalculationmethodid"
        const val NAME = "genetic_characteristic_calculation_method"
        const val ORDER = "genetic_characteristic_calculation_method_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
