package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Company
import com.weyr_associates.animaltrakkerfarmmobile.model.PredefinedNote
import com.weyr_associates.animaltrakkerfarmmobile.model.UserType

object PredefinedNoteTable {

    const val NAME = "predefined_notes_table"

    fun predefinedNoteFromCursor(cursor: Cursor): PredefinedNote {
        val contactId = cursor.getEntityId(Columns.CONTACT_ID)
        val companyId = cursor.getEntityId(Columns.COMPANY_ID)
        return PredefinedNote(
            id = cursor.getEntityId(Columns.ID),
            text = cursor.getString(Columns.NOTE_TEXT),
            userId = when {
                contactId.isValid -> contactId
                companyId.isValid -> companyId
                else -> throw IllegalStateException("Unable to determine user id. Contact and company are both 0.")
            },
            userType = when {
                contactId.isValid -> UserType.CONTACT
                companyId.isValid -> UserType.COMPANY
                else -> throw IllegalStateException("Unable to determine user type. Contact and company are both 0.")
            },
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_predefinednotesid"
        const val NOTE_TEXT = "predefined_note_text"
        const val CONTACT_ID = "id_contactid"
        const val COMPANY_ID = "id_companyid"
        const val ORDER = "predefined_note_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {

        const val QUERY_PREDEFINED_NOTES_ALL =
            """SELECT * FROM ${PredefinedNoteTable.NAME}"""

        const val ORDER_BY_PREDEFINED_NOTE_ORDER =
            """ORDER BY ${PredefinedNoteTable.Columns.ORDER} ASC"""

        const val QUERY_PREDEFINED_NOTES_FOR_CONTACT_USER_AND_DEFAULTS =
            """$QUERY_PREDEFINED_NOTES_ALL
                WHERE ${PredefinedNoteTable.Columns.CONTACT_ID} = ?
                    OR ${PredefinedNoteTable.Columns.COMPANY_ID} = ${Company.ID_GENERIC}
                $ORDER_BY_PREDEFINED_NOTE_ORDER"""

        const val QUERY_PREDEFINED_NOTES_FOR_COMPANY_USER_AND_DEFAULTS =
            """$QUERY_PREDEFINED_NOTES_ALL
                WHERE ${PredefinedNoteTable.Columns.COMPANY_ID} = ?
                    OR ${PredefinedNoteTable.Columns.COMPANY_ID} = ${Company.ID_GENERIC}
                $ORDER_BY_PREDEFINED_NOTE_ORDER"""

        const val QUERY_PREDEFINED_NOTES_DEFAULTS_ONLY =
            """$QUERY_PREDEFINED_NOTES_ALL
                WHERE ${PredefinedNoteTable.Columns.COMPANY_ID} = ${Company.ID_GENERIC}
                $ORDER_BY_PREDEFINED_NOTE_ORDER
            """

        const val QUERY_PREDEFINED_NOTE_BY_ID =
            """$QUERY_PREDEFINED_NOTES_ALL
                WHERE ${PredefinedNoteTable.Columns.ID} = ?"""
    }
}
