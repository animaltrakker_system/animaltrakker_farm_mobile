package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalDrugTable {

    const val NAME = "animal_drug_table"

    object Columns {
        const val ID = "id_animaldrugid"
        const val ANIMAL_ID = "id_animalid"
        const val DRUG_LOT_ID = "id_druglotid"
        const val DATE_ON = "drug_date_on"
        const val TIME_ON = "drug_time_on"
        const val DATE_OFF = "drug_date_off"
        const val TIME_OFF = "drug_time_off"
        const val DOSAGE = "drug_dosage"
        const val OFF_LABEL_DRUG_ID = "id_drugofflabelid"
        const val LOCATION_ID = "id_druglocationid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
