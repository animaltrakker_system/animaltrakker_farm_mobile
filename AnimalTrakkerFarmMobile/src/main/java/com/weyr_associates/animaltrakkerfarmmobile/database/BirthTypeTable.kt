package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.BirthType
import com.weyr_associates.animaltrakkerfarmmobile.model.RearType

object BirthTypeTable {

    const val NAME = "birth_type_table"

    fun birthTypeFromCursor(cursor: Cursor): BirthType {
        return BirthType(
            id = cursor.getEntityId(BirthTypeTable.Columns.ID),
            name = cursor.getString(BirthTypeTable.Columns.NAME),
            abbreviation = cursor.getString(BirthTypeTable.Columns.ABBREVIATION),
            order = cursor.getInt(BirthTypeTable.Columns.ORDER)
        )
    }

    fun rearTypeFromCursor(cursor: Cursor): RearType {
        return RearType(
            id = cursor.getEntityId(BirthTypeTable.Columns.ID),
            name = cursor.getString(BirthTypeTable.Columns.NAME),
            abbreviation = cursor.getString(BirthTypeTable.Columns.ABBREVIATION),
            order = cursor.getInt(BirthTypeTable.Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_birthtypeid"
        const val NAME = "birth_type"
        const val ABBREVIATION = "birth_type_abbrev"
        const val ORDER = "birth_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"

        const val REAR_TYPE_NAME = "rear_type_name"
    }

    object Sql {
        const val QUERY_ALL_BIRTH_TYPES =
            """SELECT * FROM ${BirthTypeTable.NAME}
                ORDER BY ${BirthTypeTable.Columns.ORDER}"""
    }
}
