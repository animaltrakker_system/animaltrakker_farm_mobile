package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.ScrapieFlockNumber

object ScrapieFlockNumberTable {

    const val NAME = "scrapie_flock_number_table"

    fun scrapieFlockNumberFromCursor(cursor: Cursor): ScrapieFlockNumber {
        return ScrapieFlockNumber(
            id = cursor.getEntityId(ScrapieFlockNumberTable.Columns.ID),
            number = cursor.getString(ScrapieFlockNumberTable.Columns.NUMBER),
            order = cursor.getInt(ScrapieFlockNumberTable.Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_scrapieflocknumberid"
        const val NUMBER = "scrapie_flockid"
        const val ORDER = "scrapie_flock_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_SCRAPIE_FLOCK_NUMBER_FROM_NUMBER =
            """SELECT * FROM ${ScrapieFlockNumberTable.NAME}
                WHERE ${ScrapieFlockNumberTable.Columns.NUMBER} = ?
                LIMIT 1"""
    }
}
