package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.ServiceType

object ServiceTypeTable {

    const val NAME = "service_type_table"

    fun serviceTypeFromCursor(cursor: Cursor): ServiceType {
        return ServiceType(
            id = cursor.getEntityId(ServiceTypeTable.Columns.ID),
            name = cursor.getString(ServiceTypeTable.Columns.NAME),
            abbreviation = cursor.getString(ServiceTypeTable.Columns.ABBREVIATION),
            order = cursor.getInt(ServiceTypeTable.Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_servicetypeid"
        const val NAME = "service_type"
        const val ABBREVIATION = "service_abbrev"
        const val ORDER = "service_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_ALL_SERVICE_TYPES =
            """SELECT * FROM ${ServiceTypeTable.NAME}
                ORDER BY ${ServiceTypeTable.Columns.ORDER}"""

        const val QUERY_SERVICE_TYPE_BY_ID =
            """SELECT * FROM ${ServiceTypeTable.NAME}
                WHERE ${ServiceTypeTable.Columns.ID} = ?"""
    }
}
