package com.weyr_associates.animaltrakkerfarmmobile.database

object DrugLotTable {

    const val NAME = "drug_lot_table"

    object Columns {
        const val ID = "id_druglotid"
        const val DRUG_ID = "id_drugid"
        const val LOT = "drug_lot"
        const val EXPIRATION_DATE = "drug_expire_date"
        const val PURCHASE_DATE = "drug_purchase_date"
        const val AMOUNT_PURCHASED = "drug_amount_purchased"
        const val COST = "drug_cost"
        const val COST_UNITS_ID = "id_drug_cost_id_unitsid"
        const val DISPOSE_DATE = "drug_dispose_date"
        const val IS_GONE = "drug_gone"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
