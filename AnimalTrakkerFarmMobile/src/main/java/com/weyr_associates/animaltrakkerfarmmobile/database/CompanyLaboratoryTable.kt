package com.weyr_associates.animaltrakkerfarmmobile.database

object CompanyLaboratoryTable {

    const val NAME = "company_laboratory_table"

    object Columns {
        const val ID = "id_companylaboratoryid"
        const val COMPANY_ID = "id_companyid"
        const val LICENSE_NUMBER = "lab_license_number"
        const val ORDER = "laboratory_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
