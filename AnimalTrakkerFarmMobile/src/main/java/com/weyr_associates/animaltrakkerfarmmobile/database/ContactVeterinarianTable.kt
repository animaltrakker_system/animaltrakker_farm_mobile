package com.weyr_associates.animaltrakkerfarmmobile.database

object ContactVeterinarianTable {

    const val NAME = "contact_veterinarian_table"

    object Columns {
        const val ID = "id_contactveterinarianid"
        const val CONTACT_ID = "id_contactid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
