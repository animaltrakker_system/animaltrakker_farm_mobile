package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Trait

object EvalTraitTable {

    const val NAME = "evaluation_trait_table"

    fun traitFromCursor(cursor: Cursor): Trait {
        return Trait(
            id = cursor.getEntityId(Columns.ID),
            name = cursor.getString(Columns.NAME),
            typeId = cursor.getEntityId(Columns.TYPE_ID),
            unitsTypeId = cursor.getEntityId(Columns.UNITS_TYPE_ID),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_evaluationtraitid"
        const val NAME = "trait_name"
        const val TYPE_ID = "id_evaluationtraittypeid"
        const val UNITS_TYPE_ID = "id_unitstypeid"
        const val ORDER = "evaluation_trait_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_TRAITS_BY_TYPE =
            """SELECT * FROM ${EvalTraitTable.NAME}
                WHERE ${Columns.TYPE_ID} = ?
                ORDER BY ${Columns.ORDER}"""
    }
}
