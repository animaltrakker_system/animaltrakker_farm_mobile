package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueTest

object TissueTestTable {

    const val NAME = "tissue_test_table"

    fun tissueTestFrom(cursor: Cursor): TissueTest {
        return TissueTest(
            id = cursor.getEntityId(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_tissuetestid"
        const val NAME = "tissue_test_name"
        const val ABBREVIATION = "tissue_test_abbrev"
        const val ORDER = "tissue_test_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val SQL_QUERY_TISSUE_TESTS =
            """SELECT * FROM ${TissueTestTable.NAME}
                ORDER BY ${TissueTestTable.Columns.ORDER}"""

        const val SQL_QUERY_TISSUE_TEST_BY_ID =
            """SELECT * FROM ${TissueTestTable.NAME}
                WHERE ${TissueTestTable.Columns.ID} = ?"""
    }
}
