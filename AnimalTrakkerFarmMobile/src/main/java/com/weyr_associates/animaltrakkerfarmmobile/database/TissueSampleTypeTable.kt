package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleType

object TissueSampleTypeTable {

    const val NAME = "tissue_sample_type_table"

    fun tissueSampleTypeFrom(cursor: Cursor): TissueSampleType {
        return TissueSampleType(
            id = cursor.getEntityId(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_tissuesampletypeid"
        const val NAME = "tissue_sample_type_name"
        const val ABBREVIATION = "tissue_sample_type_abbrev"
        const val ORDER = "tissue_sample_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val SQL_QUERY_TISSUE_SAMPLE_TYPES =
            """SELECT * FROM ${TissueSampleTypeTable.NAME}
                ORDER BY ${TissueSampleTypeTable.Columns.ORDER}"""

        const val SQL_QUERY_TISSUE_SAMPLE_TYPE_BY_ID =
            """SELECT * FROM ${TissueSampleTypeTable.NAME}
                WHERE ${TissueSampleTypeTable.Columns.ID} = ?"""
    }

}
