package com.weyr_associates.animaltrakkerfarmmobile.database

import com.weyr_associates.animaltrakkerfarmmobile.model.Codon

class GeneticCodonTable private constructor(val codon: Codon) {

    companion object {
        fun from(codon: Codon): GeneticCodonTable = GeneticCodonTable(codon)
    }

    @Suppress("PropertyName")
    val NAME = "genetic_codon${codon.code}_table"

    @Suppress("PropertyName")
    val Columns = CodonTableColumns(codon)

    class CodonTableColumns(codon: Codon) {
        @Suppress("PropertyName")
        val ID = "id_geneticcodon${codon.code}id"

        @Suppress("PropertyName")
        val ALLELES = "codon${codon.code}_alleles"

        val ORDER = "codon${codon.code}_display_order"

        @Suppress("PropertyName")
        val CREATED get() = "created"

        @Suppress("PropertyName")
        val MODIFIED get() = "modified"
    }
}
