package com.weyr_associates.animaltrakkerfarmmobile.database

object GeneticHornTypeTable {

    const val NAME = "genetic_horn_type_table"

    object Columns {
        const val ID = "id_genetichorntypeid"
        const val REGISTRY_COMPANY_ID = "id_registry_id_companyid"
        const val HORN_TYPE = "horn_type"
        const val HORN_TYPE_ABBREVIATION = "horn_type_abbrev"
        const val ORDER = "horn_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
