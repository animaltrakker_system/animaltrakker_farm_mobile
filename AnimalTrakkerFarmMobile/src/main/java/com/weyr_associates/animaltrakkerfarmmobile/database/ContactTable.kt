package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Contact

object ContactTable {

    const val NAME = "contact_table"

    fun contactFromCursor(cursor: Cursor): Contact {
        return Contact(
            id = cursor.getEntityId(Columns.ID),
            firstName = cursor.getString(Columns.FIRST_NAME),
            middleName = cursor.getString(Columns.MIDDLE_NAME),
            lastName = cursor.getString(Columns.LAST_NAME),
            titleId = cursor.getEntityId(Columns.TITLE_ID)
        )
    }

    object Columns {
        const val ID = "id_contactid"
        const val LAST_NAME = "contact_last_name"
        const val FIRST_NAME = "contact_first_name"
        const val MIDDLE_NAME = "contact_middle_name"
        const val TITLE_ID = "id_contacttitleid"
        const val CREATED = "created"
        const val MODIFIED = "modified"

        const val FULL_NAME_ALIAS = "contact_full_name"
        const val FULL_NAME_PROJECTION = """$NAME.$FIRST_NAME || ' ' || $NAME.$LAST_NAME"""
    }
}
