package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalGeneticCharacteristicTable {

    const val NAME = "animal_genetic_characteristic_table"

    object Columns {
        const val ID = "id_animalgeneticcharacteristicid"
        const val ANIMAL_ID = "id_animalid"
        const val TABLE_ID = "id_geneticcharacteristictableid"
        const val VALUE_ID = "id_geneticcharacteristicvalueid"
        const val CALCULATION_ID = "id_geneticcharacteristiccalculationmethodid"
        const val DATE = "genetic_characteristic_date"
        const val TIME = "genetic_characteristic_time"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
