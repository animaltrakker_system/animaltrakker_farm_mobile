package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalMaleBreedingTable {

    const val NAME = "animal_male_breeding_table"

    object Columns {
        const val ID = "id_animalmalebreedingid"
        const val ANIMAL_ID = "id_animalid"
        const val DATE_IN = "date_male_in"
        const val TIME_IN = "time_male_in"
        const val DATE_OUT = "date_male_out"
        const val TIME_OUT = "time_male_out"
        const val SERVICE_TYPE_ID = "id_servicetypeid"
        const val STORED_SEMEN_ID = "id_animalstoredsemenid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
