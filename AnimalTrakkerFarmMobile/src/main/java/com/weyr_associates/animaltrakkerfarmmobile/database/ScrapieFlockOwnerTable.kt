package com.weyr_associates.animaltrakkerfarmmobile.database

object ScrapieFlockOwnerTable {

    const val NAME = "scrapie_flock_owner_table"

    object Columns {
        const val ID = "id_scrapieflockownerid"
        const val SCRAPIE_FLOCK_ID = "id_scrapieflocknumberid"
        const val OWNER_CONTACT_ID = "owner_id_contactid"
        const val OWNER_COMPANY_ID = "owner_id_companyid"
        const val NOTE = "owner_scrapie_flock_note"
        const val START_USE_DATE = "start_scrapie_flock_use"
        const val END_USE_DATE = "end_scrapie_flock_use"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
