package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdRemoveReason

object IdRemoveReasonTable {

    const val NAME = "id_remove_reason_table"

    fun idRemoveReasonFrom(cursor: Cursor): IdRemoveReason {
        return IdRemoveReason(
            id = cursor.getEntityId(Columns.ID),
            text = cursor.getString(Columns.REMOVE_REASON),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_idremovereasonid"
        const val REMOVE_REASON = "id_remove_reason"
        const val ORDER = "id_remove_reason_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_ALL_ID_REMOVE_REASONS =
            """SELECT * FROM ${IdRemoveReasonTable.NAME}
                WHERE ${Columns.ID} != ${IdRemoveReason.ID_CORRECT_TAG_DATA_RAW}
                ORDER BY ${Columns.ORDER}"""
    }
}
