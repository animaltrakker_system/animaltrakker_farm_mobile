package com.weyr_associates.animaltrakkerfarmmobile.database

import android.content.ContentValues
import com.weyr_associates.animaltrakkerfarmmobile.model.EntityId

fun ContentValues.putOrNullish(columnName: String, value: Int, shouldPutValue: Boolean) {
    if (shouldPutValue) {
        put(columnName, value)
    } else {
        put(columnName, Sql.NULLISH)
    }
}

fun ContentValues.putOrNullish(columnName: String, value: Int, shouldPutValue: () -> Boolean) {
    putOrNullish(columnName, value, shouldPutValue())
}

fun ContentValues.put(key: String, value: EntityId) {
    put(key, value.raw)
}

fun ContentValues.putOrNull(key: String, value: EntityId?) {
    value?.let { put(key, it) } ?: putNull(key)
}
