package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalAlertTable {

    const val NAME = "animal_alert_table"

    object Columns {
        const val ID = "id_animalalertid"
        const val ALERT_TYPE_ID = "id_alerttypeid"
        const val ANIMAL_ID = "id_animalid"
        const val ALERT_CONTENT = "alert"
        const val ALERT_DATE = "alert_date"
        const val ALERT_TIME = "alert_time"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
