package com.weyr_associates.animaltrakkerfarmmobile.database

object GeneticCharacteristicTable {

    const val NAME = "genetic_characteristic_table"

    object Columns {
        const val ID = "id_geneticcharacteristicid"
        const val TABLE_NAME = "genetic_characteristic_table_name"
        const val TABLE_DISPLAY_NAME = "genetic_characteristic_table_display_name"
        const val TABLE_DISPLAY_ORDER = "genetic_characteristic_table_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
