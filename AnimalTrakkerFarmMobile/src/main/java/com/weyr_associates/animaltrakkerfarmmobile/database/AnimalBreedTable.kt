package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalBreedTable {

    const val NAME = "animal_breed_table"

    object Columns {
        const val ID = "id_animalbreedid"
        const val ANIMAL_ID = "id_animalid"
        const val BREED_ID = "id_breedid"
        const val BREED_PERCENTAGE = "breed_percentage"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
