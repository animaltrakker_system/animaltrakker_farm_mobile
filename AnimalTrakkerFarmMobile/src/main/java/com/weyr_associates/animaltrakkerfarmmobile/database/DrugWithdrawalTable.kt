package com.weyr_associates.animaltrakkerfarmmobile.database

object DrugWithdrawalTable {

    const val NAME = "drug_withdrawal_table"

    object Columns {
        const val ID = "id_drugwithdrawalid"
        const val DRUG_ID = "id_drugid"
        const val SPECIES_ID = "id_speciesid"
        const val MEAT_WITHDRAWAL = "drug_meat_withdrawal"
        const val MEAT_WITHDRAWAL_UNITS_ID = "id_meat_withdrawal_id_unitsid"
        const val USER_MEAT_WITHDRAWAL = "user_meat_withdrawal"
        const val MILK_WITHDRAWAL = "drug_milk_withdrawal"
        const val MILK_WITHDRAWAL_UNITS_ID = "id_milk_withdrawal_id_unitsid"
        const val USER_MILK_WITHDRAWAL = "user_milk_withdrawal"
        const val OFFICIAL_DRUG_DOSAGE = "official_drug_dosage"
        const val USER_DRUG_DOSAGE = "user_drug_dosage"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
