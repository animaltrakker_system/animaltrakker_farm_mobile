package com.weyr_associates.animaltrakkerfarmmobile.database

object OffLabelDrugTable {

    const val NAME = "drug_off_label_table"

    object Columns {
        const val ID = "id_drugofflabelid"
        const val DRUG_ID = "id_drugid"
        const val SPECIES_ID = "id_speciesid"
        const val OFF_LABEL_VET_CONTACT_ID = "off_label_id_contactveterinarianid"
        const val OFF_LABEL_DRUG_DOSAGE = "off_label_drug_dosage"
        const val OFF_LABEL_USE_START = "start_off_label_use"
        const val OFF_LABEL_USE_END = "end_off_label_use"
        const val OFF_LABEL_NOTE = "off_label_note"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
