package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Drug

object DrugTable {

    const val NAME = "drug_table"

    fun drugFromCursor(cursor: Cursor): Drug {
        return Drug(
            id = cursor.getEntityId(Columns.ID),
            typeId = cursor.getEntityId(Columns.TYPE_ID),
            tradeName = cursor.getString(Columns.TRADE_NAME),
            genericName = cursor.getString(Columns.GENERIC_NAME),
            isRemovable = cursor.getBoolean(Columns.IS_REMOVABLE),
        )
    }

    object Columns {
        const val ID = "id_drugid"
        const val TYPE_ID = "id_drugtypeid"
        const val TRADE_NAME = "trade_drug_name"
        const val GENERIC_NAME = "generic_drug_name"
        const val IS_REMOVABLE = "drug_removable"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {

        const val QUERY_ALL_DRUGS =
            """SELECT * FROM ${DrugTable.NAME}
                ORDER BY LOWER(${DrugTable.Columns.TRADE_NAME})"""

        const val QUERY_DRUGS_BY_TYPE =
            """SELECT * FROM ${DrugTable.NAME}
                WHERE ${DrugTable.Columns.TYPE_ID} = ?
                ORDER BY LOWER(${DrugTable.Columns.TRADE_NAME})"""

        const val QUERY_DRUG_BY_ID =
            """SELECT * FROM ${DrugTable.NAME}
                WHERE ${DrugTable.Columns.ID} = ?"""
    }
}
