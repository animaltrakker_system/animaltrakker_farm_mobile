package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalFemaleBreedingTable {

    const val NAME = "animal_female_breeding_table"

    object Columns {
        const val ID = "id_animalfemalebreedingid"
        const val ANIMAL_ID = "id_animalid"
        const val BIRTHING_DATE = "birthing_date"
        const val BIRTHING_TIME = "birthing_time"
        const val BIRTHING_NOTES = "birthing_notes"
        const val NUMBER_ANIMALS_BORN = "number_animals_born"
        const val NUMBER_ANIMALS_WEANED = "number_animals_weaned"
        const val GESTATION_LENGTH = "gestation_length"
        const val MALE_BREEDING_ID = "id_animalmalebreedingid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
