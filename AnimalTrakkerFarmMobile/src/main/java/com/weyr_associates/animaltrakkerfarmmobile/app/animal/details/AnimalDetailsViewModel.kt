package com.weyr_associates.animaltrakkerfarmmobile.app.animal.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.AnimalAlertsViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalDrugHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalEvaluationsHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalGeneticsViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalNotesViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalTissueSampleHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalTissueTestHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalAlert
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalDetails
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalDrugEvent
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalEvaluation
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalGeneticCharacteristic
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalNote
import com.weyr_associates.animaltrakkerfarmmobile.model.EntityId
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleEvent
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueTestEvent
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.stateIn

class AnimalDetailsViewModel(
    private val animalId: EntityId,
    private val animalRepository: AnimalRepository
) : ViewModel(),
    AnimalGeneticsViewModelContract,
    AnimalAlertsViewModelContract,
    AnimalNotesViewModelContract,
    AnimalDrugHistoryViewModelContract,
    AnimalTissueSampleHistoryViewModelContract,
    AnimalTissueTestHistoryViewModelContract,
    AnimalEvaluationsHistoryViewModelContract
{
    sealed interface AnimalDetailsState
    data object AnimalDetailsLoading : AnimalDetailsState
    data object AnimalDetailsNotFound : AnimalDetailsState
    data class AnimalDetailsLoaded(val animalDetails: AnimalDetails) : AnimalDetailsState

    val animalDetails = flow {
        emit(AnimalDetailsLoading)
        animalRepository.queryAnimalDetailsByAnimalId(animalId)?.let { animalDetails ->
            emit(AnimalDetailsLoaded(animalDetails))
        } ?: emit(AnimalDetailsNotFound)
    }.stateIn(viewModelScope, SharingStarted.Lazily, AnimalDetailsLoading)

    override val animalGenetics: StateFlow<List<AnimalGeneticCharacteristic>?> = animalDetails.mapLatest { animalDetailsState ->
        mapFromAnimalDetailsState(animalDetailsState) { animalId -> animalRepository.queryGeneticCharacteristics(animalId).geneticCharacteristics }
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    override val animalAlerts: StateFlow<List<AnimalAlert>?> = animalDetails.mapLatest { animalDetailsState ->
        mapFromAnimalDetailsState(animalDetailsState, animalRepository::queryAnimalAlerts)
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    override val animalNoteHistory: StateFlow<List<AnimalNote>?> = animalDetails.mapLatest { animalDetailsState ->
        mapFromAnimalDetailsState(animalDetailsState, animalRepository::queryAnimalNoteHistory)
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    override val animalDrugHistory: StateFlow<List<AnimalDrugEvent>?> = animalDetails.mapLatest { animalDetailsState ->
        mapFromAnimalDetailsState(animalDetailsState, animalRepository::queryAnimalDrugHistory)
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    override val tissueSampleEventHistory: StateFlow<List<TissueSampleEvent>?> = animalDetails.mapLatest { animalDetailsState ->
        mapFromAnimalDetailsState(animalDetailsState, animalRepository::queryAnimalTissueSampleHistory)
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    override val tissueTestEventHistory: StateFlow<List<TissueTestEvent>?> = animalDetails.mapLatest { animalDetailsState ->
        mapFromAnimalDetailsState(animalDetailsState, animalRepository::queryAnimalTissueTestHistory)
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    override val animalEvaluationsHistory: StateFlow<List<AnimalEvaluation>?> = animalDetails.mapLatest { animalDetailsState ->
        mapFromAnimalDetailsState(animalDetailsState, animalRepository::queryAnimalEvaluationHistory)
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    private suspend fun <T> mapFromAnimalDetailsState(
        animalDetailsState: AnimalDetailsState,
        producer: suspend (animalId: EntityId) -> List<T>
    ): List<T>? {
        return when (animalDetailsState) {
            AnimalDetailsLoading -> null
            AnimalDetailsNotFound -> emptyList()
            is AnimalDetailsLoaded -> producer.invoke(
                animalDetailsState.animalDetails.id
            )
        }
    }
}
