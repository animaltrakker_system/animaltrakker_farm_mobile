package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.input.simple

import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.input.IdInput

data class IdInputs(
    val idInput1: IdInput,
    val idInput2: IdInput,
    val idInput3: IdInput
)