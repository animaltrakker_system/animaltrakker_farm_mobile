package com.weyr_associates.animaltrakkerfarmmobile.app.animal.action

import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.weyr_associates.animaltrakkerfarmmobile.R

abstract class AnimalActionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected val checkBoxDrawableComplete: Drawable by lazy {
        requireNotNull(
            ContextCompat.getDrawable(
                itemView.context,
                R.drawable.ic_check_box_checked
            )
        )
    }
    protected val checkBoxDrawableIncomplete: Drawable by lazy {
        requireNotNull(
            ContextCompat.getDrawable(
                itemView.context,
                R.drawable.ic_check_box_outline_blank
            )
        )
    }
    protected val backgroundDrawableComplete: Drawable by lazy {
        requireNotNull(
            ContextCompat.getDrawable(
                itemView.context,
                R.drawable.background_action_item_complete
            )
        )
    }
    protected val backgroundDrawableIncomplete: Drawable by lazy {
        requireNotNull(
            ContextCompat.getDrawable(
                itemView.context,
                R.drawable.background_action_item_incomplete
            )
        )
    }
    protected val backgroundDrawableDisabled: Drawable by lazy {
        requireNotNull(
            ContextCompat.getDrawable(
                itemView.context,
                R.drawable.background_action_item_disabled
            )
        )
    }
}
