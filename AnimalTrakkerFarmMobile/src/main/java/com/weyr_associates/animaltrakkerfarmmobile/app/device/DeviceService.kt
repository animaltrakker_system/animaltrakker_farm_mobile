package com.weyr_associates.animaltrakkerfarmmobile.app.device

object DeviceService {
    const val MSG_REGISTER_CLIENT: Int = 1
    const val MSG_UNREGISTER_CLIENT: Int = 2
    const val MSG_UPDATE_STATUS: Int = 3
    const val MSG_THREAD_SUICIDE: Int = 997
}
