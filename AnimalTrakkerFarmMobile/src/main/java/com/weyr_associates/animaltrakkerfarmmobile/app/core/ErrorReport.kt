package com.weyr_associates.animaltrakkerfarmmobile.app.core

import android.content.Context
import android.os.Parcelable
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.files.AppDirectories
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.files.Files
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import java.io.File
import java.io.PrintWriter
import java.time.LocalDateTime

@Parcelize
data class ErrorReport(
    val action: String,
    val summary: String,
    val error: Throwable? = null,
    val timeStamp: LocalDateTime = LocalDateTime.now()
) : Parcelable {

    suspend fun writeToFile(context: Context): File {
        val reportDirectory = AppDirectories.errorReportsDirectory(context)
        val fileNameTimeStamp = timeStamp.format(Files.TIME_STAMP_FORMAT_IN_FILE_NAME)
        val reportFile = File(reportDirectory, "ErrorReport-${fileNameTimeStamp}.txt")
        writeToFile(reportFile)
        return reportFile
    }

    private suspend fun writeToFile(file: File) {
        withContext(Dispatchers.IO) {
            PrintWriter(file).use { printWriter ->
                with(printWriter) {
                    appendLine("")
                    appendLine()
                    appendLine("Action:")
                    appendLine(action)
                    appendLine()
                    appendLine("Summary:")
                    appendLine(summary)
                    appendLine()
                    appendLine("Stack Trace:")
                    appendLine(
                        error?.stackTraceToString()
                            ?: "No stack trace information available."
                    )
                }
            }
        }
    }
}
