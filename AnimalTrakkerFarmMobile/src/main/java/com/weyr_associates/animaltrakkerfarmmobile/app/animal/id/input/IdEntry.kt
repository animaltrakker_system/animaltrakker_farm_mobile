package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.input

import com.weyr_associates.animaltrakkerfarmmobile.model.IdBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.EntityId

data class IdEntry(
    val id: EntityId = EntityId.UNKNOWN,
    val number: String,
    val type: IdType,
    val color: IdColor,
    val location: IdLocation,
    val isOfficial: Boolean = false,
)

fun IdBasicInfo.asIdEntry(): IdEntry {
    return IdEntry(
        id = id,
        number = number,
        type = extractIdType(),
        color = extractIdColor(),
        location = extractIdLocation(),
        isOfficial = isOfficial
    )
}
