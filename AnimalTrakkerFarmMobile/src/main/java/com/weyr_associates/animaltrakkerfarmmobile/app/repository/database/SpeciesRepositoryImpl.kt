package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.app.repository.SpeciesRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.SpeciesTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.EntityId
import com.weyr_associates.animaltrakkerfarmmobile.model.Species
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SpeciesRepositoryImpl(private val databaseHandler: DatabaseHandler) : SpeciesRepository {

    override suspend fun queryAllSpecies(): List<Species> {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SpeciesTable.Sql.SQL_QUERY_ALL_SPECIES,
                emptyArray()
            ).use { cursor ->
                cursor.readAllItems(SpeciesTable::speciesFromCursor)
            }
        }
    }

    override suspend fun querySpeciesById(id: EntityId): Species? {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                SpeciesTable.Sql.SQL_QUERY_SPECIES_BY_ID,
                arrayOf(id.toString())
            ).use { cursor ->
                cursor.readFirstItem(SpeciesTable::speciesFromCursor)
            }
        }
    }
}
