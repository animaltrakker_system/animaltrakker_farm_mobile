package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Contact

interface ContactRepository {
    fun queryContacts(): List<Contact>
    fun queryVeterinarians(): List<Contact>
    fun queryContact(id: Int): Contact?
}
