package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.IdLocationTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.IdLocationRepository
import com.weyr_associates.animaltrakkerfarmmobile.model.EntityId

class IdLocationRepositoryImpl(private val databaseHandler: DatabaseHandler) : IdLocationRepository {

    companion object {
        const val SQL_QUERY_ID_LOCATIONS =
            """SELECT * FROM ${IdLocationTable.NAME}
                ORDER BY ${IdLocationTable.Columns.ORDER}"""

        const val SQL_QUERY_ID_LOCATION_BY_ID =
            """SELECT * FROM ${IdLocationTable.NAME}
                WHERE ${IdLocationTable.Columns.ID} = ?"""
    }

    override fun queryIdLocations(): List<IdLocation> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_ID_LOCATIONS, null)
            .use { it.readAllItems(IdLocationTable::idLocationFromCursor) }
    }

    override fun queryIdLocation(id: EntityId): IdLocation? {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_ID_LOCATION_BY_ID, arrayOf(id.toString()))
            .use { it.readFirstItem(IdLocationTable::idLocationFromCursor) }
    }
}
