package com.weyr_associates.animaltrakkerfarmmobile.model

interface HasAbbreviation {
    val abbreviation: String
}
