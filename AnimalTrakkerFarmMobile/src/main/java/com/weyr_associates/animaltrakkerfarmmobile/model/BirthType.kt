package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BirthType(
    override val id: EntityId,
    override val name: String,
    val abbreviation: String,
    val order: Int
) : Parcelable, HasIdentity, HasName {
    companion object {

        const val ID_UNKNOWN_RAW = 42L
        val ID_UNKNOWN = EntityId(ID_UNKNOWN_RAW)
        val ID_SINGLE = EntityId(1)
        val ID_TWIN = EntityId(2)
        val ID_TRIPLET = EntityId(3)
        val ID_QUADRUPLET = EntityId(4)
        val ID_QUINTUPLET = EntityId(5)
        val ID_SEXTUPLET = EntityId(6)
        val ID_SEVEN = EntityId(7)
        val ID_EIGHT = EntityId(8)
        val ID_NINE = EntityId(9)
        val ID_TEN = EntityId(10)
        val ID_ELEVEN = EntityId(11)
        val ID_TWELVE = EntityId(12)
        val ID_THIRTEEN = EntityId(13)
        val ID_FOURTEEN = EntityId(14)
        val ID_FIFTEEN = EntityId(15)
        val ID_SIXTEEN = EntityId(16)

        const val BIRTH_TYPE_MISSING = "Missing Birth Type"

        fun mapOffspringCountToBirthType(count: Int): EntityId {
            return when (count) {
                1 -> ID_SINGLE
                2 -> ID_TWIN
                3 -> ID_TRIPLET
                4 -> ID_QUADRUPLET
                5 -> ID_QUINTUPLET
                6 -> ID_SEXTUPLET
                7 -> ID_SEVEN
                8 -> ID_EIGHT
                9 -> ID_NINE
                10 -> ID_TEN
                11 -> ID_ELEVEN
                12 -> ID_TWELVE
                13 -> ID_THIRTEEN
                14 -> ID_FOURTEEN
                15 -> ID_FIFTEEN
                16 -> ID_SIXTEEN
                else -> ID_UNKNOWN
            }
        }
    }
}
