package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UnitOfMeasure(
    override val id: EntityId,
    override val name: String,
    override val abbreviation: String,
    val type: Type,
    val order: Int = Int.MAX_VALUE
) : Parcelable, HasIdentity, HasName, HasAbbreviation {
    companion object {

        val SALE_PRICE_US_DOLLARS = EntityId(8)

        val TIME_UNIT_DAYS = EntityId(5)
        val TIME_UNIT_HOURS = EntityId(6)
        val TIME_UNIT_YEARS = EntityId(21)
        val TIME_UNIT_MONTHS = EntityId(22)
        val TIME_UNIT_WEEKS = EntityId(23)
        val TIME_UNIT_SECONDS = EntityId(24)

        val NONE = UnitOfMeasure(
            id = EntityId.UNKNOWN,
            name = "None",
            abbreviation = "None",
            type = Type.NONE,
            order = Int.MAX_VALUE
        )
    }

    @Parcelize
    data class Type(
        val id: EntityId,
        val name: String,
        val order: Int = Int.MAX_VALUE
    ) : Parcelable {
        companion object {
            val NONE = Type(
                id = EntityId.UNKNOWN,
                name = "None",
                order = Int.MAX_VALUE
            )
            val ID_WEIGHT = EntityId(1)
            val ID_CURRENCY = EntityId(3)
            val ID_TIME = EntityId(4)
        }
        val isWeight: Boolean
            get() = when (id) {
                ID_WEIGHT -> true
                else -> false
            }
    }
}
