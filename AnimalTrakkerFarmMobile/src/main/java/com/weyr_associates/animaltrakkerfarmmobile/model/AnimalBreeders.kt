package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AnimalBreeders(
    val animalId: Int,
    val breederId: Int,
    val breederType: Breeder.Type,
    val breederName: String,
    val sireId: Int,
    val sireBreederId: Int,
    val sireBreederType: Breeder.Type,
    val sireBreederName: String,
    val damId: Int,
    val damBreederId: Int,
    val damBreederType: Breeder.Type,
    val damBreederName: String
) : Parcelable
