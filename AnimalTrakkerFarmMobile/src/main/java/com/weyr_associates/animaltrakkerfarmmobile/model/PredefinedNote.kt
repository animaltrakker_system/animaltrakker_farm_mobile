package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PredefinedNote(
    override val id: EntityId,
    val text: String,
    val userId: EntityId,
    val userType: UserType,
    val order: Int
): Parcelable, HasIdentity {
    companion object {

        val ID_TRIMMED_HOOVES_ALL = EntityId(14)
        val ID_TRIMMED_HOOF_FRONT_LEFT = EntityId(153)
        val ID_TRIMMED_HOOF_FRONT_RIGHT = EntityId(154)
        val ID_TRIMMED_HOOF_HIND_LEFT = EntityId(155)
        val ID_TRIMMED_HOOF_HIND_RIGHT = EntityId(156)

        val ID_FOOT_ROT_HOOVES_ALL = EntityId(163)
        val ID_FOOT_ROT_HOOF_FRONT_LEFT = EntityId(164)
        val ID_FOOT_ROT_HOOF_FRONT_RIGHT = EntityId(165)
        val ID_FOOT_ROT_HOOF_HIND_LEFT = EntityId(166)
        val ID_FOOT_ROT_HOOF_HIND_RIGHT = EntityId(167)

        val ID_FOOT_SCALD_HOOVES_ALL = EntityId(158)
        val ID_FOOT_SCALD_HOOF_FRONT_LEFT = EntityId(159)
        val ID_FOOT_SCALD_HOOF_FRONT_RIGHT = EntityId(160)
        val ID_FOOT_SCALD_HOOF_HIND_LEFT = EntityId(161)
        val ID_FOOT_SCALD_HOOF_HIND_RIGHT = EntityId(162)

        val ID_HORN_QUALITY_GOOD_ALL = EntityId(32)
        val ID_HORN_QUALITY_BAD_ALL = EntityId(33)
        val ID_HORN_QUALITY_BAD_LEFT = EntityId(12)
        val ID_HORN_QUALITY_BAD_RIGHT = EntityId(11)

        val ID_HORN_SAWED_LEFT = EntityId(10)
        val ID_HORN_SAWED_RIGHT = EntityId(9)

        val ID_SHORN = EntityId(23)
        val ID_SHOD = EntityId(152)
        val ID_WEANED = EntityId(157)
    }
}
