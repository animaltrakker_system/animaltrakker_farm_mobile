package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeathReason(
    override val id: EntityId,
    val reason: String,
    val userId: EntityId,
    val userType: UserType,
    val order: Int
) : Parcelable, HasIdentity {
    companion object {
        const val ID_DEATH_REASON_UNKNOWN_RAW = 14L
        const val ID_DEATH_REASON_STILLBORN_RAW = 19L
        val ID_DEATH_REASON_UNKNOWN = EntityId(ID_DEATH_REASON_UNKNOWN_RAW)
        val ID_DEATH_REASON_STILLBORN = EntityId(ID_DEATH_REASON_STILLBORN_RAW)
        const val DEATH_REASON_MISSING = "Death Reason Missing"
    }
}
