package com.weyr_associates.animaltrakkerfarmmobile.model

import java.time.LocalDate
import java.time.LocalTime

data class AnimalMaleBreeding(
    val id: EntityId,
    val animalId: EntityId,
    val dateIn: LocalDate,
    val timeIn: LocalTime,
    val dateOut: LocalDate?,
    val timeOut: LocalTime?,
    val serviceType: ServiceType
)
