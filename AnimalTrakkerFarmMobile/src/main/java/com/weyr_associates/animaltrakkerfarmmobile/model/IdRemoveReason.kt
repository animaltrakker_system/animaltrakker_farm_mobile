package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdRemoveReason(
    override val id: EntityId,
    val text: String,
    val order: Int
) : Parcelable, HasIdentity {
    companion object {
        const val ID_CORRECT_TAG_DATA_RAW = 8L
        val ID_CORRECT_TAG_DATA = EntityId(ID_CORRECT_TAG_DATA_RAW)
    }
}
