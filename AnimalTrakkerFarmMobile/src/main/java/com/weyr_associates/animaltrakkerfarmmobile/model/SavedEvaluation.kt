package com.weyr_associates.animaltrakkerfarmmobile.model

data class SavedEvaluation(
    override val id: EntityId,
    override val name: String,
    val userId: EntityId,
    val userType: UserType,
    val summarizeInAlert: Boolean = false,
    val trait01: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait02: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait03: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait04: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait05: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait06: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait07: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait08: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait09: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait10: BasicEvalTrait = BasicEvalTrait.EMPTY,
    val trait11: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait12: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait13: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait14: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait15: UnitsEvalTrait = UnitsEvalTrait.EMPTY,
    val trait16: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait17: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait18: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait19: CustomEvalTrait = CustomEvalTrait.EMPTY,
    val trait20: CustomEvalTrait = CustomEvalTrait.EMPTY,
): HasIdentity, HasName {
    companion object {
        val ID_SIMPLE_LAMBING = EntityId(1)
        val ID_SIMPLE_SORT = EntityId(2)
        val ID_OPTIMAL_AG_RAM_TEST = EntityId(4)
        val ID_OPTIMAL_LIVESTOCK_EWE_ULTRASOUND = EntityId(5)
        val ID_SUCK_REFLEX = EntityId(6)
        val ID_SIMPLE_BIRTHS = EntityId(7)
    }
}
