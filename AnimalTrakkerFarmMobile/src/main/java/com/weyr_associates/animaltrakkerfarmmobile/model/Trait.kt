package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Trait(
    override val id: EntityId,
    override val name: String,
    val typeId: EntityId,
    val unitsTypeId: EntityId,
    val order: Int
) : Parcelable, HasIdentity, HasName {
    companion object {
        val TYPE_ID_BASIC = EntityId(1)
        val TYPE_ID_UNIT = EntityId(2)
        val TYPE_ID_OPTION = EntityId(3)
    }
}
