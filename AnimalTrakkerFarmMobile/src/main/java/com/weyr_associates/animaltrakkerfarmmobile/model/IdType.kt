package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdType(
    override val id: EntityId,
    override val name: String,
    override val abbreviation: String,
    val order: Int
) : Parcelable, HasIdentity, HasName, HasAbbreviation {

    companion object {

        const val ID_TYPE_ID_NAME_RAW = 8L
        const val ID_TYPE_ID_EID_RAW = 2L

        val ID_TYPE_ID_FED = EntityId(1)
        val ID_TYPE_ID_EID = EntityId(ID_TYPE_ID_EID_RAW)
        val ID_TYPE_ID_PAINT = EntityId(3)
        val ID_TYPE_ID_FARM = EntityId(4)
        val ID_TYPE_ID_TATTOO = EntityId(5)
        val ID_TYPE_ID_SPLIT = EntityId(6)
        val ID_TYPE_ID_NOTCH = EntityId(7)
        val ID_TYPE_ID_NAME = EntityId(ID_TYPE_ID_NAME_RAW)
        val ID_TYPE_ID_FREEZE_BRAND = EntityId(9)
        val ID_TYPE_ID_TRICH = EntityId(10)
        val ID_TYPE_ID_NUES = EntityId(11)
        val ID_TYPE_ID_SALE_ORDER = EntityId(12)
        val ID_TYPE_ID_BANGS = EntityId(13)
        val ID_TYPE_ID_UNKNOWN = EntityId(14)
        val ID_TYPE_ID_CARCASS_TAG = EntityId(15)
        val ID_TYPE_ID_FED_CANADIAN = EntityId(16)

        fun isOfficialId(id: EntityId): Boolean = when (id) {
            ID_TYPE_ID_FED,
            ID_TYPE_ID_NUES,
            ID_TYPE_ID_BANGS -> true
            else -> false
        }
    }
}
