package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.LocalDate

@Parcelize
data class AnimalWeight(
    val animalId: Int,
    val weight: Float,
    val unitsId: Int,
    val unitsName: String,
    val unitsAbbreviation: String,
    val weighedOn: LocalDate
) : Parcelable
