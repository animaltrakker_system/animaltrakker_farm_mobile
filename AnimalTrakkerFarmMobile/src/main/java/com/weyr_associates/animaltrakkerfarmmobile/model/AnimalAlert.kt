package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.LocalDate
import java.time.LocalTime

@Parcelize
sealed interface AnimalAlert : Parcelable, HasIdentity {
    override val id: EntityId
    val alertType: Type
    val animalId: Int
    val eventDate: LocalDate
    val eventTime: LocalTime
    enum class Type(val typeId: Int) {
        USER_DEFINED(1),
        DRUG_WITHDRAWAL(2),
        EVALUATION_SUMMARY(3)
    }
}

@Parcelize
data class UserDefinedAlert(
    override val id: EntityId,
    override val animalId: Int,
    override val eventDate: LocalDate,
    override val eventTime: LocalTime,
    val content: String,
) : AnimalAlert {
    override val alertType: AnimalAlert.Type
        get() = AnimalAlert.Type.USER_DEFINED
}

@Parcelize
data class DrugWithdrawalAlert(
    override val id: EntityId,
    override val animalId: Int,
    override val eventDate: LocalDate,
    override val eventTime: LocalTime,
    val drugWithdrawal: DrugWithdrawal
) : AnimalAlert {
    override val alertType: AnimalAlert.Type
        get() = AnimalAlert.Type.DRUG_WITHDRAWAL
}

@Parcelize
data class EvaluationSummaryAlert(
    override val id: EntityId,
    override val animalId: Int,
    override val eventDate: LocalDate,
    override val eventTime: LocalTime,
    val evaluationSummary: EvaluationSummary
) : AnimalAlert {
    override val alertType: AnimalAlert.Type
        get() = AnimalAlert.Type.EVALUATION_SUMMARY
}
