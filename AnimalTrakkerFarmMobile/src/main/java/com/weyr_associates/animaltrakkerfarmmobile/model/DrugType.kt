package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DrugType(
    override val id: EntityId,
    override val name: String,
    val order: Int
) : Parcelable, HasIdentity, HasName {
    companion object {
        val ID_DEWORMER = EntityId(1)
        val ID_VACCINE = EntityId(2)
        val ID_ANTIBIOTIC = EntityId(3)
        val ID_HORMONE = EntityId(4)
        val ID_COCCIDIOSTAT = EntityId(5)
        val ID_FEED_SUPPLEMENT = EntityId(6)
        val ID_ANALGESIC = EntityId(7)
    }
}
