package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@JvmInline
@Parcelize
@Serializable
value class EntityId(val raw: Long) : Comparable<EntityId>, Parcelable {

    companion object {
        val UNKNOWN = EntityId(0)
    }

    val isValid: Boolean get() = raw != 0L

    override fun toString(): String = raw.toString()

    override fun compareTo(other: EntityId): Int {
        return raw.compareTo(other.raw)
    }
}
