package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sex(
    override val id: EntityId,
    override val name: String,
    override val abbreviation: String,
    val standard: String,
    val standardAbbreviation: String,
    val order: Int,
    val speciesId: EntityId
) : Parcelable, HasIdentity, HasName, HasAbbreviation {
    companion object {
        val ID_SHEEP_RAM = EntityId(1)
        val ID_SHEEP_EWE = EntityId(2)
        val ID_SHEEP_WETHER = EntityId(3)
        val ID_SHEEP_UNKNOWN = EntityId(4)
        val ID_GOAT_BUCK = EntityId(5)
        val ID_GOAT_DOE = EntityId(6)
        val ID_GOAT_WETHER = EntityId(7)
        val ID_GOAT_UNKNOWN = EntityId(8)
        val ID_CATTLE_BULL = EntityId(9)
        val ID_CATTLE_COW = EntityId(10)
        val ID_CATTLE_STEER = EntityId(11)
        val ID_CATTLE_UNKNOWN = EntityId(12)
        val ID_HORSE_STALLION = EntityId(13)
        val ID_HORSE_MARE = EntityId(14)
        val ID_HORSE_GELDING = EntityId(15)
        val ID_HORSE_UNKNOWN = EntityId(16)
        val ID_DONKEY_JACK = EntityId(17)
        val ID_DONKEY_JENNY = EntityId(18)
        val ID_DONKEY_GELDING = EntityId(19)
        val ID_DONKEY_UNKNOWN = EntityId(20)
        val ID_PIG_BOAR = EntityId(21)
        val ID_PIG_SOW = EntityId(22)
        val ID_PIG_BARROW = EntityId(23)
        val ID_PIG_UNKNOWN = EntityId(24)

        fun speciesIdFromSexId(sexId: EntityId): EntityId = when (sexId) {
            ID_SHEEP_RAM,
            ID_SHEEP_EWE,
            ID_SHEEP_WETHER,
            ID_SHEEP_UNKNOWN -> Species.ID_SHEEP
            ID_GOAT_BUCK,
            ID_GOAT_DOE,
            ID_GOAT_WETHER,
            ID_GOAT_UNKNOWN -> Species.ID_GOAT
            ID_CATTLE_BULL,
            ID_CATTLE_COW,
            ID_CATTLE_STEER,
            ID_CATTLE_UNKNOWN -> Species.ID_CATTLE
            ID_HORSE_STALLION,
            ID_HORSE_MARE,
            ID_HORSE_GELDING,
            ID_HORSE_UNKNOWN -> Species.ID_HORSE
            ID_DONKEY_JACK,
            ID_DONKEY_JENNY,
            ID_DONKEY_GELDING,
            ID_DONKEY_UNKNOWN -> Species.ID_DONKEY
            ID_PIG_BOAR,
            ID_PIG_SOW,
            ID_PIG_BARROW,
            ID_PIG_UNKNOWN -> Species.ID_PIG
            else -> EntityId.UNKNOWN
        }

        fun isFemale(sexId: EntityId): Boolean = when (sexId) {
            ID_SHEEP_EWE,
            ID_GOAT_DOE,
            ID_CATTLE_COW,
            ID_HORSE_MARE,
            ID_DONKEY_JENNY,
            ID_PIG_SOW -> true
            else -> false
        }

        fun isMale(sexId: EntityId): Boolean = when (sexId) {
            ID_SHEEP_RAM,
            ID_GOAT_BUCK,
            ID_CATTLE_BULL,
            ID_HORSE_STALLION,
            ID_DONKEY_JACK,
            ID_PIG_BOAR -> true
            else -> false
        }

        fun isCastrate(sexId: EntityId): Boolean = when (sexId) {
            ID_SHEEP_WETHER,
            ID_GOAT_WETHER,
            ID_CATTLE_STEER,
            ID_HORSE_GELDING,
            ID_DONKEY_GELDING,
            ID_PIG_BARROW -> true
            else -> false
        }
    }
}
