package com.weyr_associates.animaltrakkerfarmmobile.model

object Animal {

    val ID_UNKNOWN_SIRE_SHEEP = EntityId(14754)
    val ID_UNKNOWN_SIRE_GOAT = EntityId(14854)
    val ID_UNKNOWN_SIRE_CATTLE = EntityId(14856)
    val ID_UNKNOWN_SIRE_HORSE = EntityId(14858)
    val ID_UNKNOWN_SIRE_DONKEY = EntityId(14860)
    val ID_UNKNOWN_SIRE_PIG = EntityId(14862)

    val ID_UNKNOWN_DAM_SHEEP = EntityId(14755)
    val ID_UNKNOWN_DAM_GOAT = EntityId(14855)
    val ID_UNKNOWN_DAM_CATTLE = EntityId(14857)
    val ID_UNKNOWN_DAM_HORSE = EntityId(14859)
    val ID_UNKNOWN_DAM_DONKEY = EntityId(14861)
    val ID_UNKNOWN_DAM_PIG = EntityId(14863)

    fun isUnknownAnimal(animalId: EntityId): Boolean {
        return when (animalId) {
            ID_UNKNOWN_SIRE_SHEEP,
            ID_UNKNOWN_SIRE_GOAT,
            ID_UNKNOWN_SIRE_CATTLE,
            ID_UNKNOWN_SIRE_HORSE,
            ID_UNKNOWN_SIRE_DONKEY,
            ID_UNKNOWN_SIRE_PIG,
            ID_UNKNOWN_DAM_SHEEP,
            ID_UNKNOWN_DAM_GOAT,
            ID_UNKNOWN_DAM_CATTLE,
            ID_UNKNOWN_DAM_HORSE,
            ID_UNKNOWN_DAM_DONKEY,
            ID_UNKNOWN_DAM_PIG -> true
            else -> false
        }
    }
}
