package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class EvalTraitOption(
    override val id: EntityId,
    val traitId: EntityId,
    override val name: String,
    val order: Int = Int.MAX_VALUE
) : Parcelable, HasIdentity, HasName {
    companion object {
        val ID_SIMPLE_SORT_KEEP = EntityId(40)
        val ID_SIMPLE_SORT_SHIP = EntityId(41)
        val ID_SIMPLE_SORT_CULL = EntityId(42)
        val ID_SIMPLE_SORT_OTHER = EntityId(43)

        val ID_CUSTOM_SCROTAL_PALPATION_SATISFACTORY = EntityId(44)

        val ID_SUCK_REFLEX_UNTESTED = EntityId(24)
        val ID_LAMB_EASE_UNASSISTED = EntityId(9)
    }
}
