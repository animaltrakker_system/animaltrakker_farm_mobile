package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Premise(
    override val id: EntityId
) : Parcelable, HasIdentity {
    companion object {
        val ID_PREMISE_UNKNOWN = EntityId(82)
    }
}
