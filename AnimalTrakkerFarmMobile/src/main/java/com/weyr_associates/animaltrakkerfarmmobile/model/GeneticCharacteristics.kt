package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_136_AA
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_136_AV
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_136_A_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_136_VV
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_136_V_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_136___
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_141_FF
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_141_FL
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_141_F_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_141_LL
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_141_L_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_141___
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_154_HH
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_154_H_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_154_RH
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_154_RR
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_154_R_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_154___
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_171_QQ
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_171_QR
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_171_Q_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_171_RR
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_171_R_
import com.weyr_associates.animaltrakkerfarmmobile.model.CodonCharacteristic.Companion.ID_171___
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime


@Parcelize
data class AnimalGeneticCharacteristics(
    val animalId: EntityId,
    val geneticCharacteristics: List<AnimalGeneticCharacteristic>
) : Parcelable

@Parcelize
data class AnimalGeneticCharacteristic(
    val id: EntityId,
    val geneticCharacteristicId: EntityId,
    val geneticCharacteristicValueId: EntityId,
    val geneticCharacteristic: GeneticCharacteristic,
    val calculationMethod: GeneticCharacteristic.CalculationMethod,
    val date: LocalDate,
    val time: LocalTime
) : Parcelable {
    @IgnoredOnParcel
    val dateTime: LocalDateTime by lazy {
        date.atTime(time)
    }
}

sealed interface GeneticCharacteristic : Parcelable {
    companion object {
        val ID_CODON_112 = EntityId(1)
        val ID_CODON_136 = EntityId(2)
        val ID_CODON_141 = EntityId(3)
        val ID_CODON_154 = EntityId(4)
        val ID_CODON_171 = EntityId(5)
        val ID_HORN_TYPE = EntityId(6)
        val ID_COAT_COLOR = EntityId(7)
    }

    enum class Type(val id: Int) {
        CODON(0),
        COAT_COLOR(1),
        HORN_TYPE(2)
    }

    @Parcelize
    data class CalculationMethod(
        val id: EntityId,
        val name: String,
        val order: Int = -1
    ) : Parcelable {

        companion object {

            const val ID_DNA_RAW = 1L
            val ID_DNA = EntityId(ID_DNA_RAW)

            const val ID_PEDIGREE_RAW = 2L
            val ID_PEDIGREE = EntityId(ID_PEDIGREE_RAW)

            const val ID_OBSERVATION_RAW = 3L
            val ID_OBSERVATION = EntityId(ID_OBSERVATION_RAW)
        }
    }

    val id: EntityId
    val name: String
    val type: Type
}

enum class Codon(val id: EntityId, val code: String) {
    CODE_112(GeneticCharacteristic.ID_CODON_112, "112"),
    CODE_136(GeneticCharacteristic.ID_CODON_136, "136"),
    CODE_141(GeneticCharacteristic.ID_CODON_141, "141"),
    CODE_154(GeneticCharacteristic.ID_CODON_154, "154"),
    CODE_171(GeneticCharacteristic.ID_CODON_171, "171");

    companion object {
        fun fromId(id: EntityId): Codon? {
            return entries.firstOrNull { it.id == id }
        }
    }
}

@Parcelize
data class CodonCharacteristic(
    override val id: EntityId,
    override val name: String,
    val codon: Codon,
    val alleles: String
) : GeneticCharacteristic, Parcelable {

    @IgnoredOnParcel
    override val type: GeneticCharacteristic.Type
        get() = GeneticCharacteristic.Type.CODON

    companion object {

        //  Constant         id_codon136id   codon136alleles
        //  ------------------------------------------------
        //  ID_136_AA        1               AA
        //  ID_136_A_        2               A?
        //  ID_136_AV        3               AV
        //  ID_136_V_        4               V?
        //  ID_136_VV        5               VV
        //  ID_136___        6               ??

        val ID_136_AA = EntityId(1)
        val ID_136_A_ = EntityId(2)
        val ID_136_AV = EntityId(3)
        val ID_136_V_ = EntityId(4)
        val ID_136_VV = EntityId(5)
        val ID_136___ = EntityId(6)

        //  Constant         id_codon141id   codon141alleles
        //  ------------------------------------------------
        //  ID_141___        1               ??
        //  ID_141_FF        2               FF
        //  ID_141_F_        3               F?
        //  ID_141_FL        4               FL
        //  ID_141_L_        5               L?
        //  ID_141_LL        6               LL

        val ID_141___ = EntityId(1)
        val ID_141_FF = EntityId(2)
        val ID_141_F_ = EntityId(3)
        val ID_141_FL = EntityId(4)
        val ID_141_L_ = EntityId(5)
        val ID_141_LL = EntityId(6)

        //  Constant         id_codon154id   codon154alleles
        //  ------------------------------------------------
        //  ID_154___        1               ??
        //  ID_154_RR        2               RR
        //  ID_154_RH        3               RH
        //  ID_154_R_        4               R?
        //  ID_154_HH        5               HH
        //  ID_154_H_        6               H?

        val ID_154___ = EntityId(1)
        val ID_154_RR = EntityId(2)
        val ID_154_RH = EntityId(3)
        val ID_154_R_ = EntityId(4)
        val ID_154_HH = EntityId(5)
        val ID_154_H_ = EntityId(6)

        //  Constant         id_codon171id   codon171alleles
        //  ------------------------------------------------
        //  ID_171_QQ        1               QQ
        //  ID_171_Q_        2               Q?
        //  ID_171_QR        3               QR
        //  ID_171_R_        4               R?
        //  ID_171_RR        5               RR
        //  ID_171___        6               ??
        //  ID_171_HH        7               HH
        //  ID_171_QH        8               QH
        //  ID_171_H_        9               H?
        //  ID_171_RH        10              RH
        //  ID_171_KK        11              KK
        //  ID_171_QK        12              QK
        //  ID_171_RK        13              RK
        //  ID_171_K_        14              K?
        //  ID_171_HK        15              HK

        //  Although we can code for all the alleles the US only concerns itself with Q and R
        //  Because current research is that H and K are like Q in terms of scrapie susceptibility
        //  that is what we are testing for now.
        //  Further research could allow for changes and the code can be modified then

        val ID_171_QQ = EntityId(1)
        val ID_171_Q_ = EntityId(2)
        val ID_171_QR = EntityId(3)
        val ID_171_R_ = EntityId(4)
        val ID_171_RR = EntityId(5)
        val ID_171___ = EntityId(6)
        val ID_171_HH = EntityId(7)
        val ID_171_QH = EntityId(8)
        val ID_171_H_ = EntityId(9)
        val ID_171_RH = EntityId(10)
        val ID_171_KK = EntityId(11)
        val ID_171_QK = EntityId(12)
        val ID_171_RK = EntityId(13)
        val ID_171_K_ = EntityId(14)
        val ID_171_HK = EntityId(15)
    }
}
@Parcelize
data class CoatColorCharacteristic(
    override val id: EntityId,
    override val name: String,
    val coatColor: String,
    val coatColorAbbreviation: String
) : GeneticCharacteristic, Parcelable, HasName {
    @IgnoredOnParcel
    override val type: GeneticCharacteristic.Type
        get() = GeneticCharacteristic.Type.COAT_COLOR
}

@Parcelize
data class HornTypeCharacteristic(
    override val id: EntityId,
    override val name: String,
    val hornType: String,
    val hornTypeAbbreviation: String
) : GeneticCharacteristic, Parcelable {
    @IgnoredOnParcel
    override val type: GeneticCharacteristic.Type
        get() = GeneticCharacteristic.Type.HORN_TYPE
}

object CodonCalculation {

    fun calculateCodon136(sireCodon136: EntityId?, damCodon136: EntityId?): EntityId {
        return when (damCodon136) {
            ID_136_AA -> when (sireCodon136) {
                ID_136_AA -> ID_136_AA
                ID_136_VV -> ID_136_AV
                ID_136_A_,
                ID_136_AV,
                ID_136_V_,
                ID_136___ -> ID_136_A_
                else -> ID_136_A_
            }
            ID_136_VV -> when (sireCodon136) {
                ID_136_AA -> ID_136_AV
                ID_136_VV -> ID_136_VV
                ID_136_A_,
                ID_136_AV,
                ID_136_V_,
                ID_136___ -> ID_136_V_
                else -> ID_136_V_
            }
            ID_136_A_, ID_136_V_ -> when (sireCodon136) {
                ID_136_AA -> ID_136_A_
                ID_136_A_,
                ID_136_AV,
                ID_136_V_ -> ID_136___
                ID_136_VV,
                ID_136___ -> ID_136_V_
                else -> ID_136_V_
            }
            ID_136_AV -> when (sireCodon136) {
                ID_136_AA -> ID_136_A_
                ID_136_A_,
                ID_136_AV,
                ID_136_V_ -> ID_136___
                ID_136_VV -> ID_136_V_
                ID_136___ -> ID_136___
                else -> ID_136___
            }
            else -> ID_136___ //TODO: Follow DAM __ branch???
        }
    }

    fun calculateCodon141(sireCodon141: EntityId?, damCodon141: EntityId?): EntityId {
        return when (damCodon141) {
            ID_141___ -> when (sireCodon141) {
                ID_141___ -> ID_141___
                ID_141_FF -> ID_141_F_
                ID_141_F_,
                ID_141_FL,
                ID_141_L_ -> ID_141___
                ID_141_LL -> ID_141_L_
                else -> ID_141___
            }
            ID_141_FF -> when (sireCodon141) {
                ID_141___ -> ID_141_F_
                ID_141_FF -> ID_141_FF
                ID_141_F_,
                ID_141_FL,
                ID_141_L_ -> ID_141_FL
                ID_141_LL -> ID_141_FL
                else -> ID_141_F_
            }
            ID_141_LL -> when (sireCodon141) {
                ID_141___ -> ID_141_L_
                ID_141_FF -> ID_141_FL
                ID_141_F_,
                ID_141_FL,
                ID_141_L_ -> ID_141_L_
                ID_141_LL -> ID_141_L_
                else -> ID_141_L_
            }
            ID_141_F_, ID_141_L_ -> when (sireCodon141) {
                ID_141___ -> ID_141___
                ID_141_FF -> ID_141_F_
                ID_141_F_,
                ID_141_FL,
                ID_141_L_ -> ID_141___
                ID_141_LL -> ID_141_L_
                else -> ID_141___
            }
            ID_141_FL -> when (sireCodon141) {
                ID_141___,
                ID_141_F_,
                ID_141_FL,
                ID_141_L_ -> ID_141___
                ID_141_FF -> ID_141_F_
                ID_141_LL -> ID_141_L_
                else -> ID_141___
            }
            else -> ID_141___ //TODO: Follow DAM __ branch???
        }
    }

    fun calculateCodon154(sireCodon154: EntityId?, damCodon154: EntityId?): EntityId {
        return when (damCodon154) {
            ID_154___ -> when (sireCodon154) {
                ID_154___ -> ID_154___
                ID_154_RR -> ID_154_R_
                ID_154_RH,
                ID_154_R_ -> ID_154___
                ID_154_HH -> ID_154_H_
                ID_154_H_ -> ID_154___
                else -> ID_154___
            }
            ID_154_RR -> when (sireCodon154) {
                ID_154___ -> ID_154_R_
                ID_154_RR -> ID_154_RR
                ID_154_RH,
                ID_154_R_ -> ID_154_R_
                ID_154_HH -> ID_154_RH
                ID_154_H_ -> ID_154_R_
                else -> ID_154_R_
            }
            ID_154_HH -> when (sireCodon154) {
                ID_154___ -> ID_154_H_
                ID_154_RR -> ID_154_RH
                ID_154_RH,
                ID_154_R_ -> ID_154_H_
                ID_154_HH -> ID_154_HH
                ID_154_H_ -> ID_154_H_
                else -> ID_154_H_
            }
            ID_154_R_,
            ID_154_H_ -> when (sireCodon154) {
                ID_154___ -> ID_154___
                ID_154_RR -> ID_154_R_
                ID_154_RH,
                ID_154_R_ -> ID_154___
                ID_154_HH -> ID_154_H_
                ID_154_H_ -> ID_154___
                else -> ID_154___
            }
            ID_154_RH -> when (sireCodon154) {
                ID_154___ -> ID_154___
                ID_154_RR -> ID_154_R_
                ID_154_RH,
                ID_154_R_ -> ID_154___
                ID_154_HH -> ID_154_H_
                ID_154_H_ -> ID_154___
                else -> ID_154___
            }
            else -> ID_154___ //TODO: Follow DAM __ branch???
        }
    }

    fun calculateCodon171(sireCodon171: EntityId?, damCodon171: EntityId?): EntityId {
        return when (damCodon171) {
            ID_171_QQ -> when (sireCodon171) {
                ID_171_QQ -> ID_171_QQ
                ID_171_Q_,
                ID_171_QR,
                ID_171_R_ -> ID_171_Q_
                ID_171_RR -> ID_171_QR
                ID_171___ -> ID_171_Q_
                else -> ID_171_Q_
            }
            ID_171_RR -> when (sireCodon171) {
                ID_171_QQ -> ID_171_QR
                ID_171_Q_,
                ID_171_QR,
                ID_171_R_ -> ID_171_R_
                ID_171_RR -> ID_171_RR
                ID_171___ -> ID_171_R_
                else -> ID_171_R_
            }
            ID_171_Q_,
            ID_171_R_ -> when (sireCodon171) {
                ID_171_QQ -> ID_171_Q_
                ID_171_Q_,
                ID_171_QR,
                ID_171_R_ -> ID_171___
                ID_171_RR -> ID_171_R_
                ID_171___ -> ID_171_R_
                else -> ID_171_R_
            }
            ID_171_QR -> when (sireCodon171) {
                ID_171_QQ -> ID_171_Q_
                ID_171_Q_,
                ID_171_QR,
                ID_171_R_ -> ID_171___
                ID_171_RR -> ID_171_R_

                ID_171___ -> ID_171___
                else -> ID_171___
            }
            else -> ID_171___ //TODO: Follow DAM __ branch???
        }
    }
}
