package com.weyr_associates.animaltrakkerfarmmobile.model

data class ScrapieFlockNumber(
    val id: EntityId,
    val number: String,
    val order: Int
)
