package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RearType(
    override val id: EntityId,
    override val name: String,
    val abbreviation: String,
    val order: Int
) : Parcelable, HasIdentity, HasName {
    companion object {

        const val ID_UNKNOWN_RAW = 42L
        val ID_UNKNOWN = EntityId(ID_UNKNOWN_RAW)

        const val REAR_TYPE_MISSING = "Missing Rear Type"
    }
}
