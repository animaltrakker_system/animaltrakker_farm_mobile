package com.weyr_associates.animaltrakkerfarmmobile.model

import java.time.LocalDate
import java.time.LocalTime

data class AnimalFemaleBreeding(
    val id: EntityId,
    val animalId: EntityId,
    val birthingDate: LocalDate?,
    val birthingTime: LocalTime?,
    val birthingNotes: String,
    val numberOfAnimalsBorn: Int,
    val numberOfAnimalsWeaned: Int,
    val gestationLength: Int,
    val maleBreeding: AnimalMaleBreeding?
)
