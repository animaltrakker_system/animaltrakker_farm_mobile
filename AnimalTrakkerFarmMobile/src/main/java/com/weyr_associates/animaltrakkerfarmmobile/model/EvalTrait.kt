package com.weyr_associates.animaltrakkerfarmmobile.model

sealed interface EvalTrait {
    val id: EntityId
    val name: String
    val typeId: EntityId
    val order: Int
    val isEmpty: Boolean
    val isOptional: Boolean
    val isDeferred: Boolean

    companion object {
        val TRAIT_ID_LAMB_EASE = EntityId(24)
        val TRAIT_ID_SUCK_REFLEX = EntityId(37)
        val TRAIT_ID_RAM_LAMBS_BORN = EntityId(68)
        val TRAIT_ID_WETHER_LAMBS_BORN = EntityId(69)
        val TRAIT_ID_EWE_LAMBS_BORN = EntityId(70)
        val TRAIT_ID_UNK_SEX_LAMBS_BORN = EntityId(71)
        val TRAIT_ID_STILL_BORN_LAMBS_BORN = EntityId(72)
        val TRAIT_ID_ABORTED_LAMBS = EntityId(73)
        val TRAIT_ID_ADOPTED_LAMBS = EntityId(74)
        val UNIT_TRAIT_ID_SCROTAL_CIRCUMFERENCE = EntityId(15)
        val UNIT_TRAIT_ID_WEIGHT = EntityId(16)
        val UNIT_TRAIT_ID_BODY_CONDITION_SCORE = EntityId(51)
        val OPTION_TRAIT_ID_SIMPLE_SORT = EntityId(54)
        val OPTION_TRAIT_ID_CUSTOM_SCROTAL_PALPATION = EntityId(80)
    }
}

data class BasicEvalTrait(
    override val id: EntityId,
    override val name: String,
    override val typeId: EntityId,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE,
) : EvalTrait {
    companion object {
        val EMPTY = BasicEvalTrait(
            id = EntityId.UNKNOWN,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false
        )
    }
}

data class UnitsEvalTrait(
    override val id: EntityId,
    override val name: String,
    override val typeId: EntityId,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    val units: UnitOfMeasure,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE,
) : EvalTrait {
    companion object {
        val EMPTY = UnitsEvalTrait(
            id = EntityId.UNKNOWN,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false,
            units = UnitOfMeasure.NONE
        )
    }
}

data class CustomEvalTrait(
    override val id: EntityId,
    override val name: String,
    override val typeId: EntityId,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    val options: List<EvalTraitOption>,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE
) : EvalTrait {
    companion object {

        val EMPTY = CustomEvalTrait(
            id = EntityId.UNKNOWN,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false,
            options = emptyList()
        )
    }
}
