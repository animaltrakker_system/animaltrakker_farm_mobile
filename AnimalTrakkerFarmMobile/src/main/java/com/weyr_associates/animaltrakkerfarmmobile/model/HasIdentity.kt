package com.weyr_associates.animaltrakkerfarmmobile.model

interface HasIdentity {
    val id: EntityId
}
