package com.weyr_associates.animaltrakkerfarmmobile.model

data class SireInfo(
    val sireName: AnimalName,
    val serviceType: ServiceType
)
