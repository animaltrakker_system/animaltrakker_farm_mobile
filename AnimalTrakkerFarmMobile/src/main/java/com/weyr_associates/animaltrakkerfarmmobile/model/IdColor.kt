package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdColor(
    override val id: EntityId,
    override val name: String,
    override val abbreviation: String,
    val order: Int
) : Parcelable, HasIdentity, HasName, HasAbbreviation {
    companion object {
        val ID_COLOR_ID_UNKNOWN = EntityId(14)
        val ID_COLOR_ID_NOT_APPLICABLE = EntityId(15)
    }
}
