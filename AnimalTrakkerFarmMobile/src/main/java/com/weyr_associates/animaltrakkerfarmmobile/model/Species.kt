package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Species(
    override val id: EntityId,
    val commonName: String,
    val genericName: String,
    val earlyFemaleBreedingAgeDays: Int,
    val earlyGestationLengthDays: Int,
    val lateGestationLengthDays: Int,
    val typicalGestationLengthDays: Int
) : Parcelable, HasIdentity, HasName {

    @IgnoredOnParcel
    override val name: String = commonName

    companion object {

        val ID_SHEEP = EntityId(1)
        val ID_GOAT = EntityId(2)
        val ID_CATTLE = EntityId(3)
        val ID_HORSE = EntityId(4)
        val ID_DONKEY = EntityId(5)
        val ID_PIG = EntityId(6)

        fun unknownSireIdForSpecies(speciesId: EntityId): EntityId = when (speciesId) {
            ID_SHEEP -> Animal.ID_UNKNOWN_SIRE_SHEEP
            ID_GOAT -> Animal.ID_UNKNOWN_SIRE_GOAT
            ID_CATTLE -> Animal.ID_UNKNOWN_SIRE_CATTLE
            ID_HORSE -> Animal.ID_UNKNOWN_SIRE_HORSE
            ID_DONKEY -> Animal.ID_UNKNOWN_SIRE_DONKEY
            ID_PIG -> Animal.ID_UNKNOWN_SIRE_PIG
            else -> EntityId.UNKNOWN
        }

        fun unknownDamIdForSpecies(speciesId: EntityId): EntityId = when (speciesId) {
            ID_SHEEP -> Animal.ID_UNKNOWN_DAM_SHEEP
            ID_GOAT -> Animal.ID_UNKNOWN_DAM_GOAT
            ID_CATTLE -> Animal.ID_UNKNOWN_DAM_CATTLE
            ID_HORSE -> Animal.ID_UNKNOWN_DAM_HORSE
            ID_DONKEY -> Animal.ID_UNKNOWN_DAM_DONKEY
            ID_PIG -> Animal.ID_UNKNOWN_DAM_PIG
            else -> EntityId.UNKNOWN
        }

        fun isUnknownAnimal(animalId: EntityId): Boolean {
            return when (animalId) {
                Animal.ID_UNKNOWN_SIRE_SHEEP,
                Animal.ID_UNKNOWN_SIRE_GOAT,
                Animal.ID_UNKNOWN_SIRE_CATTLE,
                Animal.ID_UNKNOWN_SIRE_HORSE,
                Animal.ID_UNKNOWN_SIRE_DONKEY,
                Animal.ID_UNKNOWN_SIRE_PIG,
                Animal.ID_UNKNOWN_DAM_SHEEP,
                Animal.ID_UNKNOWN_DAM_GOAT,
                Animal.ID_UNKNOWN_DAM_CATTLE,
                Animal.ID_UNKNOWN_DAM_HORSE,
                Animal.ID_UNKNOWN_DAM_DONKEY,
                Animal.ID_UNKNOWN_DAM_PIG -> true
                else -> false
            }
        }
    }
}
