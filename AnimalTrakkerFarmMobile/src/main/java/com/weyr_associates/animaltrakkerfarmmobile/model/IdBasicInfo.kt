package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Parcelize
data class IdBasicInfo(
    override val id: EntityId,
    val number: String,
    val typeId: EntityId,
    val typeName: String,
    val typeAbbreviation: String,
    val colorId: EntityId,
    val colorName: String,
    val colorAbbreviation: String,
    val locationId: EntityId,
    val locationName: String,
    val locationAbbreviation: String,
    val isOfficial: Boolean,
    val dateOn: LocalDate,
    val timeOn: LocalTime
) : Parcelable, HasIdentity {

    object Differ : ItemCallback<IdBasicInfo>() {
        override fun areItemsTheSame(oldItem: IdBasicInfo, newItem: IdBasicInfo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: IdBasicInfo, newItem: IdBasicInfo): Boolean {
            return oldItem == newItem
        }
    }

    fun extractIdType(): IdType {
        return IdType(id = typeId, name = typeName, abbreviation = typeAbbreviation, order = 0)
    }

    fun extractIdColor(): IdColor {
        return IdColor(id = colorId, name = colorName, abbreviation = colorAbbreviation, order = 0)
    }

    fun extractIdLocation(): IdLocation {
        return IdLocation(id = locationId, name = locationName, abbreviation = locationAbbreviation, order = 0)
    }

    @IgnoredOnParcel
    val dateTimeOn: LocalDateTime by lazy {
        LocalDateTime.of(dateOn, timeOn)
    }
}

fun List<IdBasicInfo>.mostRecentDateOnOfType(idTypeId: EntityId): IdBasicInfo? {
    return this.filter { it.typeId == idTypeId }.maxByOrNull { it.dateTimeOn }
}

fun List<IdBasicInfo>.oldestDateOnOfType(idTypeId: EntityId): IdBasicInfo? {
    return this.filter { it.typeId == idTypeId }.minByOrNull { it.dateTimeOn }
}
