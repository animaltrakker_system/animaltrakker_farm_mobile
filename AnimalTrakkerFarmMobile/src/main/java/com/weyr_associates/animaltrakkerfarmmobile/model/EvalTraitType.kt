package com.weyr_associates.animaltrakkerfarmmobile.model

object EvalTraitType {
    val ID_SCORED_1_TO_5 = EntityId(1)
    val ID_REAL_VALUE = EntityId(2)
    val ID_USER_LIST = EntityId(3)
    val ID_REAL_VALUE_CALCULATED = EntityId(4)
}
