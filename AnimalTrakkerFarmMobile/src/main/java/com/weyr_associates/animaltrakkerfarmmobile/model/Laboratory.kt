package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Laboratory(
    override val id: EntityId,
    val companyId: Int,
    override val name: String,
    val licenseNumber: String,
    val order: Int
) : Parcelable, HasIdentity, HasName {
    companion object {
        val LAB_COMPANY_ID_OPTIMAL_LIVESTOCK = EntityId(660)
    }
}
